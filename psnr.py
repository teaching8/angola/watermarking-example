import io
from pathlib import Path

import fire
import imageio.v3 as iio
from almiky.metrics.imperceptibility import psnr


def main(cover_dir, watermarked_dir):
    count = 0
    psnr_sum = 0

    for path in Path(cover_dir).iterdir():
        cover = iio.imread(path)
        watermarked = iio.imread(Path(watermarked_dir) / path.name)
        psnr_sum += psnr(cover, watermarked)
        count += 1

    psnr_mean = psnr_sum / count
    print(f'Mean PSNR: {psnr_mean}')


if __name__ == '__main__':
  fire.Fire(main)
