import csv
from pathlib import Path

import fire

import utils


def main(indir, outdir):
    steps = (5 + index * 5 for index in range(30))
    results = []

    for step in steps:
        print(f'Run for step: {step}')
        output_path = Path(outdir) / str(step)
        utils.process_covers(indir, output_path, step)
        results.append([
            step,
            utils.mean_psnr(indir, output_path),
            utils.mean_uiqi(indir, output_path)
        ])

    with open('./result.csv', mode='w') as result_file:
        result_writer = csv.writer(
            result_file,
            delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL
        )
        result_writer.writerow(('Step', 'PSNR'))

        for result in results:
            result_writer.writerow(result)


if __name__ == '__main__':
  fire.Fire(main)
