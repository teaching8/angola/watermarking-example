Analysis of the impact of digital watermarking on computer-aided
diagnosis in medical imaging

# Setup
## Create and activate a virtual environment

```
$ python -m venv venv
$ source venv/bin/active
```

## Install dependencies

```
$ pip install -r requirements
```

Install almiky dependency

Clone almiky project from https://github.com/ydiazn/almiky.git

and install it

```
$ pip install -e path-to-almiky-project
```

# Run

Insert 8 bit in each 8x8 block of cover.
A random message is generated for each image.
Each cover is processed for multiples steps (dither modulation) from
5 to 150 with an increment of 5. A mean PSNR for each step is saved
in the file result.csv in project root dir


```
$ python main.py --indir=/path/to/cover/images \
    --outdir=/path/to/watermarkin/images
```
