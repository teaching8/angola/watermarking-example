import io
import os
import random
from pathlib import Path

import imageio.v3 as iio
from almiky.utils.blocks import BlocksImage
from almiky.metrics.imperceptibility import psnr, uiqi

from hider import BlockMultipleDCTDMFactory


def generate_message(cover):
    blocks = BlocksImage(cover, 8, 8)
    message = ''
    i = 0
    size = len(blocks) * 8

    while i < size:
        message += str(random.randint(0, 1))
        i += 1

    return message


def mean_psnr(cover_dir, watermarked_dir):
    count = 0
    psnr_sum = 0

    for path in Path(cover_dir).iterdir():
        cover = iio.imread(path)
        watermarked = iio.imread(Path(watermarked_dir) / path.name)
        psnr_sum += psnr(cover, watermarked)
        count += 1

    return psnr_sum / count

def mean_uiqi(cover_dir, watermarked_dir):
    count = 0
    uiqi_sum = 0

    for path in Path(cover_dir).iterdir():
        cover = iio.imread(path)
        watermarked = iio.imread(Path(watermarked_dir) / path.name)
        uiqi_sum += uiqi(cover, watermarked)
        count += 1

    return uiqi_sum / count


def process_covers(indir, outdir, step):
    block_DCT_DM = BlockMultipleDCTDMFactory().build(step, dither=0)

    for path in Path(indir).iterdir():
        print(f'Process image {path.name}')
        cover = iio.imread(path)
        message = generate_message(cover)

        for index in range(3):
            cover[:,:,index] = block_DCT_DM.insert(
                cover[:,:,index],
                message,
                bit_per_block=8,
                start_index=8,
            )

        output = io.BytesIO()
        iio.imwrite(output, cover, plugin="pillow", extension=path.suffix)
        output_dir = str(outdir / path.name)
        os.makedirs(os.path.dirname(output_dir), exist_ok=True)

        with open(output_dir, "wb") as outfile:
            outfile.write(output.getbuffer())