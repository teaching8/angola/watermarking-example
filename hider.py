import numpy as np

from almiky.utils.blocks import BlocksImage
from almiky.embedding.qim.dm import BinaryDither, BinaryDM
from almiky.hiders.base import SingleBitHider
from almiky.hiders.base import TransformHider
from almiky.hiders.block import BlockHider, BlockBitHider
from almiky.moments.matrix import ImageTransform, Transform
from almiky.moments.transform import DCT2
from almiky.quantization.scalar import UniformQuantizer
from almiky.utils.scan.scan import ScanMapping


'''
Basic hiders
'''


class MultipleBitHider:
    '''Hide a bit in one coefficient.

    Build an hider from a scanner and embedder:
        hider = MultipleBitHider(scan, embedder)

    Args:
        scan (ScanMapping): Scan Mapping
        embedder (Embedder): Embedder
    '''

    def __init__(self, scan, embedder):
        '''
        Initialize self. See help(type(self)) for accurate signature.
        '''
        self.embedder = embedder
        self.scan = scan

    def insert(self, cover_work, bits, start_index=0):
        '''
        Hide a multiples bits

        Args:
            cover_work (numpy array): cover Work array
            bits (array): array of bit to hide
            index (int): start index of coefficient where bit will be hidden
        '''
        data = np.copy(cover_work)
        scanning = self.scan(data)
        index = start_index
        #print(f'Start index: {start_index}')
        #print(f'Bits: {bits}')

        for bit in bits:
            amplitude = scanning[index]
            scanning[index] = self.embedder.embed(amplitude, bit)
            index += 1

        return data

    def extract(self, ws_work, bit_count, start_index=0):
        '''
        Get multiples bits hidden an return it

        Args:
            ws_work (numpy array): watermarked or stego Work array
            start_index (int): start index of coefficient
                where bit will be extracted (default is 0)
            bit_count (int) number of bit to extract
        '''
        bits = np.empty(bit_count)
        scanning = self.scan(ws_work)

        for index in range(bit_count):
            amplitude = scanning[index + start_index]
            bits[index] = self.embedder.extract(amplitude)

        return ''.join(bits)


class BlockMultipleBitHider(BlockHider):
    '''
    Hide a bit in one coefficient.

    Build an hider from a scanner and embedder:
    hider = MultipleBitHider(scan, embeder)

    then you can insert a bit in a coefficient
    index = 0
    hider.insert(1, index)

    or extract a bit from a coefficient
    hider.extract(10)
    '''

    def __init__(self, hider):
        '''
        Initialize self. See help(type(self)) for accurate signature.
        '''
        self.hider = hider

    def insert(self, cover, msg, block_shape=(8, 8), bit_per_block=8, **kwargs):
        '''
        Hide a bit

        Arguments:
        bit -- bit to hide
        index -- index of coefficient where bit will be hidden
        '''
        data = np.copy(cover)
        blocks = BlocksImage(data, *block_shape)
        msg_size = len(msg)
        block_num = msg_size // bit_per_block

        for index in range(block_num):
            try:
                start = index * bit_per_block
                end = start + bit_per_block
                submsg = msg[start:end]
                blocks[index] = self.hider.insert(blocks[index], submsg, **kwargs)
            except IndexError as e:
                raise ValueError("Capacity exceded.") from e

        return data

    def extract(self, ws_work, block_shape=(8, 8), bit_per_block=8, **kwargs):
        '''
        Get bit hidden an return it

        Arguments:
        index -- index of coefficient where bit will be extracted
        '''
        msg = ''
        blocks = BlocksImage(ws_work, *block_shape)

        for block in blocks:
            submsg = self.hider.extract(block, bit_count=bit_per_block, **kwargs)
            msg += submsg

        return msg


class BlockDCTDMFactory:

    def build(self, step, dither):
        '''
        Return a hider

        Args:
            step (float): step size for dither modulation
            dither (float): dither
            lm (str): left matrix name of separable moment
            rm (str): right matrix name of separable moment
        '''

        return BlockBitHider(
            TransformHider(
                SingleBitHider(
                    ScanMapping(),
                    BinaryDM(
                        UniformQuantizer(step),
                        BinaryDither(step, dither)
                    )
                ),
                ImageTransform(Transform(DCT2))
            )
        )


class BlockMultipleDCTDMFactory:

    def build(self, step, dither):
        '''
        Return a hider

        Args:
            step (float): step size for dither modulation
            dither (float): dither
            lm (str): left matrix name of separable moment
            rm (str): right matrix name of separable moment
        '''

        return BlockMultipleBitHider(
            TransformHider(
                MultipleBitHider(
                    ScanMapping(),
                    BinaryDM(
                        UniformQuantizer(step),
                        BinaryDither(step, dither)
                    )
                ),
                ImageTransform(Transform(DCT2))
            )
        )